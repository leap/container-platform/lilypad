#!/bin/bash
set -eo pipefail

GREEN='\033[0;32m'
RED='\033[0;31m'
ORANGE='\033[0;33m'
BOLD='\033[1m'
NC='\033[0m'

# make sure we're in the scripts root directory
cd $(dirname "$0")


# ensure we have a $SSH_KEY_FILE set
if [[ -f .env ]]; then
    . .env
else
    echo -e "${ORANGE}.env file not found!${NC}"
fi

if [[ -z  $SSH_KEY_FILE ]]; then
    echo -e "${RED}Please set the SSH_KEY_FILE environment variable to your private SSH key used for lilypad deployments. You can add it to your .env file${NC}"
    exit 1
fi

if [[ -z $(docker image ls -q lilypad-runner) ]]; then
    echo -e "${ORANGE}Docker image lilypad-runner is missing. Building it now...${NC}"
    docker build . -t lilypad-runner
fi

# start gpg agent deamon
gpg-agent --daemon || echo "gpg-agent already running"

# start the ssh agent deamon and add your lilypad ssh key
eval $(ssh-agent)
ssh-add $SSH_KEY_FILE

if [[ -z $SSH_AUTH_SOCK ]]; then
    echo -e "${RED}Couldn't find your ssh agent socket. Environment variable SSH_AUTH_SOCK not set${NC}"
    exit 1
fi

echo -e """
${GREEN}Please run ${NC}${BOLD}source /venv/bin/activate${NC}${GREEN} first!${NC}"""

# start docker with gpg-agent socket and ssh-agent socket mounted
# FIXME: we actually don't want to mount the whole ssh directory, but
# lilypad requires it for some reason right now
docker run -it --rm \
    -v .:/lilypad \
    -v $(gpgconf --list-dirs agent-socket):/run/user/1000/gnupg/S.gpg-agent \
    -e GPG_TTY=$(tty) \
    -v $SSH_AUTH_SOCK:/ssh-agent \
    -e SSH_AUTH_SOCK=/ssh-agent \
    -v ~/.ssh/:/root/.ssh/ \
    lilypad-runner bash
