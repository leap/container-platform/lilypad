#!/bin/bash

#!/usr/bin/env bash
set -eu

RAID_ARRAYS=$(lsblk --noheadings --output NAME,TYPE --list | awk '$2 ~ /^raid/ { print $1 }')

for MD_DEVICE in ${RAID_ARRAYS}; do
  # Subshell to avoid eval'd variables from leaking between iterations
  (
    # Query sysfs for info about md device
    SYSFS_BASE="/sys/devices/virtual/block/${MD_DEVICE}/md"
    MD_LAYOUT=$(cat "${SYSFS_BASE}/layout")
    MD_LEVEL=$(cat "${SYSFS_BASE}/level")
    MD_METADATA_VERSION=$(cat "${SYSFS_BASE}/metadata_version")
    MD_NUM_RAID_DISKS=$(cat "${SYSFS_BASE}/raid_disks")

    # Remove 'raid' prefix from RAID level
    MD_LEVEL=${MD_LEVEL#raid}

    # Output RAID array informational metric.
    # NOTE: Metadata version is a label rather than a separate metric because the version can be a string
    echo "node_md_info{md_device=\"${MD_DEVICE}\", raid_level=\"${MD_LEVEL}\", md_metadata_version=\"${MD_METADATA_VERSION}\"} 1"

    # Fetch sync state and metrics.
    SYNC_STATE=$(cat "${SYSFS_BASE}/sync_action")
    echo "node_md_sync_state{md_device=\"${MD_DEVICE}\",sync_state=\"${SYNC_STATE}\"} 1"
    SYNC_SPEED=$(cat "${SYSFS_BASE}/sync_speed")
    if [ "$SYNC_SPEED" = "none" ]; then
        SYNC_SPEED=0
    fi
    echo "node_md_sync_speed{md_device=\"${MD_DEVICE}\"} ${SYNC_SPEED}"

    DEGRADED=$(cat "${SYSFS_BASE}/degraded")
    echo "node_md_degraded{md_device=\"${MD_DEVICE}\"} ${DEGRADED}"
  )
done
