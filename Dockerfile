FROM golang:1.22 AS build

RUN go install git.autistici.org/ale/x509ca@latest
RUN go install git.autistici.org/ale/ed25519gen@latest
RUN go install git.autistici.org/ai3/go-common/cmd/pwtool@latest


FROM debian:12-slim
RUN apt-get update -qq && \
    apt-get -y dist-upgrade && \
    apt-get -y install build-essential bind9utils git libsodium23 python3.11 virtualenv gnupg2 bash && \
    apt-get clean
COPY --from=build go/bin /usr/local/bin
VOLUME ["/lilypad"]
COPY . /lilypad/
RUN virtualenv -p /usr/bin/python3 venv && \
    . ./venv/bin/activate && \
    pip install -r lilypad/requirements.txt

ENV ANSIBLE_VAULT_PASSWORD_FILE=/lilypad/.ansible_vault_pw.gpg
ENV ANSIBLE_STRATEGY_PLUGINS=/venv/lib/python3.11/site-packages/ansible_mitogen/plugins/strategy/

WORKDIR /lilypad
